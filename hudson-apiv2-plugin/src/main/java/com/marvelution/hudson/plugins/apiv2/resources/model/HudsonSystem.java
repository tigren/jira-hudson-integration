/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.hudson.plugins.apiv2.resources.model;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

import com.marvelution.hudson.plugins.apiv2.resources.utils.NameSpaceUtils;

/**
 * Enumeration with all the Hudson System options
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 * 
 * @since 4.1.0
 */
@XmlType(name = "HudsonSystemType", namespace = NameSpaceUtils.APIV2_NAMESPACE)
@XmlEnum(String.class)
public enum HudsonSystem {

	HUDSON("Hudson"), JENKINS("Jenkins");

	private final String humanName;
	private final User systemUser;

	/**
	 * Constructor
	 *
	 * @param systemHumanName the System human readable name
	 */
	private HudsonSystem(String systemHumanName) {
		humanName = systemHumanName;
		systemUser = new User("SYSTEM", systemHumanName + " System", systemHumanName + " system account.");
	}

	/**
	 * Getter for humanName
	 *
	 * @return the humanName
	 * @since 4.2.0
	 */
	public String getHumanName() {
		return humanName;
	}

	/**
	 * Getter for the System {@link User}
	 * 
	 * @return the System {@link User}
	 * @since 4.2.0
	 */
	public User getSystemUser() {
		return systemUser;
	}

	/**
	 * Getter for the {@link HudsonSystem}
	 * 
	 * @return the {@link HudsonSystem}, returns {@link HudsonSystem#JENKINS} in case the class
	 * 			<code>jenkins.model.Jenkins.class</code> can be loaded, otherwise {@link HudsonSystem#HUDSON} is
	 * 			returned.
	 * @since 5.0.0
	 */
	public static HudsonSystem getHudsonSystem() {
		HudsonSystem system = HudsonSystem.HUDSON;
		try {
			Class.forName("jenkins.model.Jenkins");
			system = HudsonSystem.JENKINS;
		} catch (ClassNotFoundException e) {
			// Oke not a Jenkins instance so keep the system set to HUDSON
		}
		return system;
	}

}
