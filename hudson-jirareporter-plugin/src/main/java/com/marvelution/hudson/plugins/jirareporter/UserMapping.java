/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.hudson.plugins.jirareporter;

import hudson.Extension;
import hudson.model.AbstractDescribableImpl;
import hudson.model.Descriptor;

import org.kohsuke.stapler.DataBoundConstructor;

/**
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 *
 * @since 5.0.0
 */
public class UserMapping extends AbstractDescribableImpl<UserMapping> {

	public final String hudsonId;
	public final String jiraId;
	
	/**
	 * Constructor
	 *
	 * @param hudsonId
	 * @param jiraId
	 */
	@DataBoundConstructor
	public UserMapping(String hudsonId, String jiraId) {
		this.hudsonId = hudsonId;
		this.jiraId = jiraId;
	}

	/**
	 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
	 *
	 * @since 5.0.0
	 */
	@Extension
	public static class DescriptorImpl extends Descriptor<UserMapping> {

		/**
		 * {@inheritDoc}
		 */
		@Override
		public String getDisplayName() {
			return "";
		}

	}

}
